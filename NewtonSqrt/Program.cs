﻿using System;

namespace NewtonSqrt
{
    public class Program
    {
        static void Main(string[] str)
        {
            do
            {
                try
                {
                    Console.Write("Введите число для вычисления корня методом Ньютона: ");
                    double numberNewton = double.Parse(Console.ReadLine());
                    if (numberNewton < 0)
                    {
                        Console.WriteLine("Нельзя вычислить корень из отрицательного числа!");
                        Environment.Exit(0);
                    }

                    Console.Write("Введите степень корня : ");
                    int n = int.Parse(Console.ReadLine());
                    if (n < 0)
                    {
                        Console.WriteLine("Нельзя вычислить корень с отрицательной степенью!");
                        Environment.Exit(0);
                    }
                    Console.Write("Введите точность для вычисления корня методом Ньютона: ");
                    double eps = double.Parse(Console.ReadLine());
                    if (eps < 0)
                    {
                        Console.WriteLine("Нельзя вычислить корень с отрицательной точностью!");
                        Environment.Exit(0);
                    }

                    var newtonResult = Newton.Sqrt(numberNewton, n, eps);
                    var mathResult = Math.Pow(numberNewton, 1d / n);

                    Console.WriteLine("Результат вычисления методом Ньютона: " + newtonResult.ToString());
                    Console.WriteLine("Результат вычисления методом Math.Pow: " + mathResult.ToString());
                    Console.WriteLine($"Difference: {Math.Abs(newtonResult - mathResult)}");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Ошибка: " + ex.Message);
                }

                Console.Write("Повторить? (Y/N) ");
            }
            while (Console.ReadKey().KeyChar.ToString().ToUpper() == "Y");
        }
    }
}