﻿using System;

namespace NewtonSqrt
{
    public static class Newton
    {
        public static double Pow(double value, int power)
        {
            if (power < 0)
            {
                return 1 / Pow(value, -power);
            }

            if (power == 0)
            {
                return 1;
            }

            if (power == 1)
            {
                return value;
            }

            return power % 2 == 0 ? Pow(value * value, power / 2) : value * Pow(value * value, (power - 1) / 2);
        }

        public static double Sqrt(double value, int power, double eps)
        {
            double result = value;
            double previousResult = 0;

            if (eps >= 1)
            {
                eps = 0.00001;
            }

            while(Math.Abs(previousResult - result) >= eps)
            {
                previousResult = result;
                result = 1.0 / power * (((power - 1) * result) + (value / Pow(result, power - 1)));               
            }

            return result;
        }
    }
}